Source: eventviews
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Sandro Knauß <hefee@debian.org>, Patrick Franz <deltaone@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               cmake (>= 3.16~),
               extra-cmake-modules (>= 6.6.0~),
               gettext,
               libakonadi-dev (>= 4:24.12.2~),
               libakonadicalendar-dev (>= 4:24.12.2~),
               libcalendarsupport-dev (>= 4:24.12.2~),
               libkcalendarutils-dev (>= 4:24.12.2~),
               libkdepim-dev (>= 4:24.12.2~),
               libkf6calendarcore-dev (>= 6.6.0~),
               libkf6codecs-dev (>= 6.6.0~),
               libkf6completion-dev (>= 6.6.0~),
               libkf6configwidgets-dev (>= 6.3.0~),
               libkf6contacts-dev (>= 6.6.0~),
               libkf6guiaddons-dev (>= 6.6.0~),
               libkf6holidays-dev (>= 6.6.0~),
               libkf6i18n-dev (>= 6.6.0~),
               libkf6iconthemes-dev (>= 6.6.0~),
               libkf6itemmodels-dev (>= 6.6.0~),
               libkf6service-dev (>= 6.6.0~),
               libkgantt-dev (>= 3.0.0~),
               libkmime-dev (>= 24.12.2~),
               libxkbcommon-dev,
               qt6-base-dev (>= 6.7.0~),
               xauth <!nocheck>,
               xvfb <!nocheck>,
Standards-Version: 4.7.0
Homepage: https://invent.kde.org/pim/eventviews
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/eventviews
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/eventviews.git
Rules-Requires-Root: no

Package: libeventviews-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Description: KDE PIM event handling - data files
 This library provides an event creator for KDE PIM.
 .
 This package provides the data files used by the library.

Package: libeventviews-dev
Section: libdevel
Architecture: amd64 arm64 armhf i386
Multi-Arch: same
Depends: libakonadi-dev (>= 4:24.12.2~),
         libakonadicalendar-dev (>= 4:24.12.2~),
         libcalendarsupport-dev (>= 4:24.12.2~),
         libkcalendarutils-dev (>= 4:24.12.2~),
         libkf6calendarcore-dev (>= 6.6.0~),
         libkpim6eventviews6 (= ${binary:Version}),
         ${misc:Depends},
Description: KDE PIM event handling - development files
 This library provides an event creator for KDE PIM.
 .
 This package provides the development files.

Package: libkpim6eventviews6
Provides: ${ABI:VirtualPackage},
Architecture: amd64 arm64 armhf i386
Multi-Arch: same
Depends: libeventviews-data (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: KDE PIM event handling - library
 This library provides an event creator for KDE PIM.
 .
 This package provides the shared libraries.
